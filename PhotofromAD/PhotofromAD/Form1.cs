﻿using System;
using System.DirectoryServices;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace PhotofromAD
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            try
            {
                string userName = txtUsername.Text;
                using (var dsSearcher = new DirectorySearcher())
                {

                    var idx = userName.IndexOf('\\');
                    if (idx > 0)
                        userName = userName.Substring(idx + 1);
                    dsSearcher.Filter = string.Format("(&(objectClass=user)(samaccountname={0}))", userName);
                    SearchResult result = dsSearcher.FindOne();
                    if (result != null)
                    {
                        using (var user = new DirectoryEntry(result.Path))
                        {
                            var data = user.Properties["thumbnailPhoto"].Value as byte[];
                            var cn = user.Properties["cn"].Value;
                            var physicalDeliveryOfficeName = user.Properties["physicalDeliveryOfficeName"].Value;
                            var description = user.Properties["description"].Value;
                            var telephoneNumber = user.Properties["telephoneNumber"].Value;
                            if (data != null)
                               
                            using (var s = new MemoryStream(data))
                               
                            pictureBox1.Image = Bitmap.FromStream(s);
                            label2.Text = cn.ToString();
                            label3.Text = description.ToString();
                            label4.Text= physicalDeliveryOfficeName.ToString();
                            label5.Text = telephoneNumber.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            MessageBox.Show(ex.Message, "Error");
            }
        }
    }

}

               

       
   

